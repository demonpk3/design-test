import {Outlet} from 'react-router-dom';
import {ConfigProvider, Layout} from 'antd';
import 'reset-css'
import classes from './index.module.scss';

const { Footer, Sider } = Layout;

const LayoutApp = () => {
  return (
    <>
      <ConfigProvider
        theme={{
          components: {
            Layout: {
              headerBg: '#0099ce',
              headerPadding: 5
            },
        }}
      }
      >
      <Layout style={{
        minHeight: "100vh"
      }} className={classes.app}>
        <Sider width={"30%"} className={classes.sider}>

        </Sider>
        <Layout>
          <Outlet />
          <Footer>

          </Footer>
        </Layout>
      </Layout>
      </ConfigProvider>
    </>
  );
};

export default LayoutApp;