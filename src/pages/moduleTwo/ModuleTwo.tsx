import {config} from './config.ts'
import {useEffect, useState} from 'react';
import {Button, Col, Image, Row, Spin} from 'antd';
import {useNavigate} from 'react-router-dom';
import Title from 'antd/es/typography/Title';
import {Content, Header} from 'antd/es/layout/layout';


const initialValue = {
  currentQuestion: 0,
  question: config
};

const ModuleTwo = () => {
  const [currentQuestion, setCurrentQuestion] = useState(initialValue.currentQuestion);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [result, setResult] = useState(initialValue);
  const navigate  = useNavigate();
  const saveAnswer = (answer: string) => {
    setIsLoading(true);
    result.question[currentQuestion] = {
      ...result.question[currentQuestion],
      userAnswer: answer,
    }
    setResult((value) => {
        const newResult = {
          currentQuestion: value.currentQuestion + 1,
          question: [
            ...value.question
          ]
        };
        localStorage.setItem('moduleTwo', JSON.stringify(newResult))
        return newResult;
      }
    );
    setCurrentQuestion(value => value + 1);
    setTimeout(()=> {setIsLoading(false)}, 200)
  };

  useEffect(() => {
    const result = localStorage.getItem('moduleTwo');
    if (result){
      try {
        const objResult = JSON.parse(result);
        setResult(objResult)
        setCurrentQuestion(objResult.currentQuestion)
      } catch (e) {
        setResult(initialValue);
        localStorage.setItem('moduleTwo', JSON.stringify(initialValue))
      }
    }
  },[])

  return (
    <>
      <Header>
        <Title style={{color: '#ecf0f1', textAlign: "center"}} level={1}>Фирменный персонаж</Title>
      </Header>
      <Content style={{display: "flex", justifyContent: "center"}}>
    <Spin size={'large'} spinning={isLoading}>
    <div>
      {result.question[result.currentQuestion] ?
        (
          <>
            <div style={{ height: "75vh", display: "flex", flexDirection: "column", alignItems: "center", justifyContent: "center" }}>
              <Image
                src={result.question[result.currentQuestion].img}
                height={"60vh"}
                style={{maxHeight: "100%", objectFit: "contain"}}
              />
              <Title>{result.question[result.currentQuestion].text}</Title>
            </div>

            <Row justify={'space-around'}>
              {result.question[result.currentQuestion].options.map((option) => (
                <Col xs={6} key={`${option.id}_${result.question[result.currentQuestion].id}`}>
                  <Button size="large" style={{
                    width: "100%"
                  }} onClick={() => { saveAnswer(option.text)}}>{option.text}</Button>
                </Col>
              ))}
            </Row>

          </>
        )
        : (
          <>
            <Row>
              <Col style={{margin: "20px"}}>
                <Title> Поздравляем с завершением второго этапа</Title>
                <Button onClick={() => { navigate('/result') }}>Узнать результат</Button>
              </Col>
            </Row>
          </>
        )
      }
    </div>
    </Spin>
      </Content>
      </>
  );
};

export default ModuleTwo;