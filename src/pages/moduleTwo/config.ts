export interface options {
  id: number;
  text: string;
}

export interface moduleTwoConfig {
  id: number,
  img: string,
  text: string,
  options: options[]
  correct: string,
  userAnswer?: string,
}

export const config: moduleTwoConfig[] = [
  {
    id: 1,
    img: '/img/module2/bugdroid.jpg',
    text: 'Как его зовут?',
    correct: 'Bugdroid',
    options: [
      { id: 1, text: 'Android' },
      { id: 2, text: 'Bugdroid'},
      { id: 3, text: 'DNS' },
      { id: 4, text: 'Ситилинк'},
    ]
  },
  {
    id: 2,
    img: '/img/module2/bugdroid.jpg',
    text: 'Какой компании он принадлежит?',
    correct: 'Android',
    options: [
      { id: 1, text: 'Android' },
      { id: 2, text: 'Bugdroid'},
      { id: 3, text: 'DNS' },
      { id: 4, text: 'Ситилинк'},
    ]
  },
  {
    id: 3,
    img: '/img/module2/g-nusmas.png',
    text: 'Как его зовут?',
    correct: 'G-nusmas',
    options: [
      { id: 1, text: 'Samsung' },
      { id: 2, text: 'Duracell'},
      { id: 3, text: 'НАСА' },
      { id: 4, text: 'G-nusmas'},
    ]
  },
  {
    id: 4,
    img: '/img/module2/g-nusmas.png',
    text: 'Какой компании он принадлежит?',
    correct: 'Samsung',
    options: [
      { id: 1, text: 'Samsung' },
      { id: 2, text: 'Duracell'},
      { id: 3, text: 'НАСА' },
      { id: 4, text: 'G-nusmas'},
    ]
  },
  {
    id: 5,
    img: '/img/module2/Snoo.png',
    text: 'Как его зовут?',
    correct: 'Snoo',
    options: [
      { id: 1, text: 'Reddit' },
      { id: 2, text: 'Алексис'},
      { id: 3, text: 'Snoo' },
      { id: 4, text: 'Huffman'},
    ]
  },
  {
    id: 6,
    img: '/img/module2/Snoo.png',
    text: 'Какой компании он принадлежит?',
    correct: 'Reddit',
    options: [
      { id: 1, text: 'Reddit' },
      { id: 2, text: 'Алексис'},
      { id: 3, text: 'Snoo' },
      { id: 4, text: 'Huffman'},
    ]
  },
  {
    id: 7,
    img: '/img/module2/spotty.png',
    text: 'Как его зовут?',
    correct: 'Спотти',
    options: [
      { id: 1, text: 'Unconnected' },
      { id: 2, text: 'Контакт'},
      { id: 3, text: 'Спотти' },
      { id: 4, text: 'Бим'},
    ]
  },
  {
    id: 8,
    img: '/img/module2/spotty.png',
    text: 'Какой компании он принадлежит?',
    correct: 'ВКонтакте',
    options: [
      { id: 1, text: 'ВКонтакте' },
      { id: 2, text: 'Фейсбук'},
      { id: 3, text: 'Одноклассники' },
      { id: 4, text: 'Mail'},
    ]
  },
]