import {Flex, Button, Form, Input} from 'antd';
import type { FormProps } from 'antd';
import {useNavigate} from 'react-router-dom';
import Title from 'antd/es/typography/Title';
import {Content, Header} from 'antd/es/layout/layout';

type FieldType = {
  username?: string;
};



const defaultValue = localStorage.getItem("username") || "";


const StartGame = () => {
  const navigate  = useNavigate();
  const onFinish: FormProps<FieldType>['onFinish'] = (values) => {
    values.username && localStorage.setItem("username", values.username);
    localStorage.setItem("attempt", "true");
    navigate('/module/1')
  };

  return (
    <>
    <Header>
      <Title style={{color: '#ecf0f1', textAlign: "center"}} level={1}></Title>
    </Header>
  <Content style={{display: "flex", justifyContent: "center"}}>
    <Flex align={'center'}>
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        style={{ maxWidth: 600 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        autoComplete="off"
      >
        <Title level={2}>Пожалуйста введите Ваше имя</Title>
        <Form.Item<FieldType>
          label="Ваше имя"
          name="username"
          initialValue={defaultValue}
          rules={[{ required: true, message: 'Пожалуйста введите Ваше имя' }]}
        >
          <Input/>
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button size={'large'} type="primary" htmlType="submit">
            Начать
          </Button>
        </Form.Item>
      </Form>
    </Flex>
  </Content>
  </>
  );
};

export default StartGame;