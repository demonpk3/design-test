export type answersType = "good" | "bad";
export interface moduleOneConfig {
  id: number,
  img: string,
  correct: answersType,
  userAnswer?: answersType,
}
export const config: moduleOneConfig[] = [
  {
    id: 1,
    img: "/img/module1/1.png",
    correct:"good",
  },
  {
    id: 2,
    img: "/img/module1/2.png",
    correct:"bad",
  },
  {
    id: 3,
    img: "/img/module1/3.png",
    correct:"good",
  },
  {
    id: 4,
    img: "/img/module1/4.png",
    correct:"bad",
  },
  {
    id: 5,
    img: "/img/module1/5.png",
    correct:"good",
  },
  {
    id: 6,
    img: "/img/module1/6.png",
    correct:"bad",
  },
  {
    id: 7,
    img: "/img/module1/7.png",
    correct:"good",
  },
  {
    id: 8,
    img: "/img/module1/8.jpg",
    correct:"good",
  },
  {
    id: 9,
    img: "/img/module1/9.png",
    correct:"bad",
  },
]