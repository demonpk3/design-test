import {answersType, config} from './config.ts'
import {useEffect, useState} from 'react';
import {Button, Col, Image, Row} from 'antd';
import {useNavigate} from 'react-router-dom';
import ModalResult from '../../components/ModalResult/ModalResult.tsx';
import Title from 'antd/es/typography/Title';
import {Content, Header} from 'antd/es/layout/layout';

const initialValue = {
  currentQuestion: 0,
  question: config
};

const ModuleOne = () => {
  const [currentQuestion, setCurrentQuestion] = useState(initialValue.currentQuestion);
  const [result, setResult] = useState(initialValue);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [countCorrect, setCountCorrect] = useState('')
  const navigate  = useNavigate();
  const saveAnswer = (answer: answersType) => {
    result.question[currentQuestion] = {
      ...result.question[currentQuestion],
      userAnswer: answer,
    }
    setResult((value) => {
      const newResult = {
        currentQuestion: value.currentQuestion + 1,
        question: [
          ...value.question
        ]
      };
      localStorage.setItem('moduleOne', JSON.stringify(newResult))
      return newResult;
    }
  );
    setCurrentQuestion(value => value + 1);
  };

  const showModal = () => {
    const count = result.question.reduce((acc, question) => {
      if (question.correct === question.userAnswer) {
        return acc + 1;
      }
      return acc;
    }, 0);
    setCountCorrect(`${count} / ${result.question.length}`)
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  useEffect(() => {
    const result = localStorage.getItem('moduleOne');
    if (result){
      try {
        const objResult = JSON.parse(result);
        setResult(objResult)
        setCurrentQuestion(objResult.currentQuestion)
      } catch (e) {
        setResult(initialValue);
        localStorage.setItem('moduleOne', JSON.stringify(initialValue))
      }
    }
  },[])

  return (
    <>
    <Header>
      <Title style={{color: '#ecf0f1', textAlign: "center"}} level={1}>Логотипы</Title>
    </Header>
  <Content style={{display: "flex", justifyContent: "center"}}>
    <div>
      {result.question[result.currentQuestion] ?
        (
          <div style={{width: "100%"}}>
            <Title>Этот логотип хороший или плохой?</Title>
            <div style={{height: "60vh", display: "flex", alignItems: "center", justifyContent: "center"}}>
              <Image
                src={result.question[result.currentQuestion].img}
                height={"60vh"}
                style={{maxHeight: "100%", objectFit: "contain"}}
              />
            </div>

            <Row justify={'space-around'} style={{paddingTop: "30px"}}>
              <Col xs={6}>
                <Button size="large" style={{
                  backgroundColor: "#44c056",
                  color: 'white',
                  width: "100%"
                }} onClick={() => {
                  saveAnswer("good")
                }}>Хороший</Button>
              </Col>
              <Col xs={6}>
                <Button size="large" style={{
                  backgroundColor: "#e12f43",
                  color: 'white',
                  width: "100%"
                }} onClick={() => {
                  saveAnswer("bad")
                }}>Плохой</Button>
              </Col>
            </Row>
          </div>
        )
        : (
          <>
            <Row>
              <Col style={{margin: "20px"}}>
                <Title> Поздравляем с завершением первого этапа</Title>
                <Button onClick={() => {
                  showModal()
                }}>Результат</Button>
                <Button onClick={() => {
                  navigate('/module/2')
                }}>Перейти на модуль 2</Button>
              </Col>
            </Row>
          </>
        )
      }
      <ModalResult result={countCorrect} open={isModalOpen} onOk={handleOk}/>
    </div>
  </Content>
    </>
)
  ;
};

export default ModuleOne;