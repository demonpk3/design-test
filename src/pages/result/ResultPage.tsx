import {useEffect, useState} from 'react';
import {useNavigate} from 'react-router-dom';
import Title from 'antd/es/typography/Title';
import {Content, Header} from 'antd/es/layout/layout';
import {moduleOneConfig} from '../moduleOne/config.ts';
import {moduleTwoConfig} from '../moduleTwo/config.ts';
import {Button} from 'antd';


interface Result<T> {
  currentQuestion?: 0,
  question: T[],
}


const ResultPage = () => {
  const navigate  = useNavigate();
  const [resultOne, setResultOne] = useState<Result<moduleOneConfig>>({question:[]});
  const [resultTwo, setResultTwo] = useState<Result<moduleTwoConfig>>({question:[]});

  const moduleOne = localStorage.getItem('moduleOne');
  const moduleTwo = localStorage.getItem('moduleTwo');

  const clearStore = () => {
    localStorage.clear()
    navigate('/')
  }
  useEffect(() => {
    try {
      if (!moduleOne) throw new Error('Нет результатов первого модуля')
      setResultOne(JSON.parse(moduleOne))
    } catch (e) {
      console.error(e)
      return navigate('/module/1')
    }

    try {
      if (!moduleTwo) throw new Error('Нет результатов первого модуля')
      setResultTwo(JSON.parse(moduleTwo));
    } catch (e) {
      console.error(e)
      return navigate('/module/2')
    }
  }, []);

  function getResultOne(){
    const count = resultOne?.question.reduce((acc: number, question) => {
      if (question.correct === question.userAnswer) {
        return acc + 1;
      }
      return acc;
    }, 0);
    return count;
  }
  function getResultTwo(){
    const count = resultTwo?.question.reduce((acc: number, question) => {
      if (question.correct === question.userAnswer) {
        return acc + 1;
      }
      return acc;
    }, 0);
    return count;
  }

  function getResult(){
    return getResultOne() + getResultTwo()
  }

  return (
    <>
      <Header>
        <Title style={{color: '#ecf0f1', textAlign: "center"}} level={1}>Логотипы</Title>
      </Header>
      <Content style={{display: "flex", justifyContent: "center"}}>
        <div style={{width: "100%", paddingInline: "20px"}}>
          <div style={{textAlign: 'center'}}>
            <Title>Поздравляю {localStorage.getItem('username')}</Title>
          </div>

          <Title>В первом задании вы набрали: {getResultOne()} / {resultOne.question.length} </Title>
          <p style={{color: 'red'}}>Допущенные ошибки:</p>
          <div style={{display: 'flex', justifyContent: 'space-around', alignItems: 'flex-end'}}>
            {resultOne.question.filter((question) => question.correct !== question.userAnswer).map((question) => (
              <div key={question.id} style={{textAlign: 'center'}}>
                <img src={question.img} style={{width: '100px'}} alt=""/>
                <p style={{color: question.correct === 'bad' ? "red" : "green"}}>{question.correct === 'bad' ? 'Это плохой логотип' : 'Это хороший логотип'}</p>
              </div>
            ))}
          </div>
          <Title>Во втором задании вы набрали: {getResultTwo()} / {resultTwo.question.length} </Title>
          <p style={{color: 'red'}}>Допущенные ошибки:</p>
          <div style={{display: 'flex', justifyContent: 'space-around', alignItems: 'flex-end', flexWrap: 'wrap'}}>
            {resultTwo.question.filter((question) => question.correct !== question.userAnswer).map((question) => (
              <div key={question.id} style={{textAlign: 'center', width: '16.6666%', marginBottom: '30px'}}>
                <img src={question.img} style={{width: '100px'}} alt=""/>
                <p>{question.text === 'Как его зовут?' ? `Его зовут: ${question.correct}` : `Он принадлежит: ${question.correct}`}</p>
              </div>
            ))}
          </div>
          <Title>Итого вы набрали: {getResult()} / {resultOne.question.length + resultTwo.question.length} </Title>
        </div>
        <Button onClick={clearStore}>Начать с начала</Button>
      </Content>
    </>
  );
};

export default ResultPage;