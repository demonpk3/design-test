export {default as StartGame} from './StartGame.tsx'
export {default as ModuleOne} from './moduleOne/ModuleOne.tsx'
export {default as ModuleTwo} from './moduleTwo/ModuleTwo.tsx'
export {default as ResultPage} from './result/ResultPage.tsx'
