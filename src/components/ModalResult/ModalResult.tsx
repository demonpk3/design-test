import {Modal} from 'antd';
import { Typography } from 'antd';

const { Title } = Typography;
interface ModalResultProps {
  open: boolean,
  onOk: ((e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void),
  result: string
}

const ModalResult = ({open, onOk, result}: ModalResultProps) => {

  return (
    <Modal title="Результаты" open={open} onOk={onOk} onCancel={onOk}>
      <Title level={1}>{result}</Title>
    </Modal>
  );
};

export default ModalResult;