import React from 'react'
import ReactDOM from 'react-dom/client'
import {createBrowserRouter, redirect, RouterProvider} from 'react-router-dom';
import LayoutApp from './layout/Layout.tsx';
import {ModuleTwo, ModuleOne, StartGame, ResultPage} from './pages';
const router = createBrowserRouter([
  {
    path: "/",
    element: <LayoutApp/>,
    children: [
      {
        index: true,
        element: <StartGame />,
        loader: () => {
          const isAttempt = localStorage.getItem("attempt") === "true";
          if (isAttempt) {
            return redirect("/module/1");
          }
          return null;
        }
      },
      {
        path: "/module/1",
        element: <ModuleOne/>,
      },
      {
        path: "/module/2",
        element: <ModuleTwo/>
      },
      {
        path: "/result",
        element: <ResultPage/>
      },
    ],
  },
]);

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
